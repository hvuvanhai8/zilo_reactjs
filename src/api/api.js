
import axios from 'axios';

export function ApiGet(url, getParams={}) {
    return axios.get(`${process.env.REACT_APP_ZILO_URL}/${url}`,{params:getParams})
        .then(response=>response.data)
        .catch(error=>{console.error(error);
        throw error;
    })
}


export function ApiPost(url,getParams={}) {
    const params = new URLSearchParams();
    Object.keys(getParams).forEach(key=>{
        params.append(key,getParams[key]);
    });
    return axios.post(`${process.env.REACT_APP_ZILO_URL}/${url}`,params).then(response=>response.data).catch(error=>{
        console.error(error);
        throw error;
    })
}

export function ApiDelete(url,getParams={}) {
    // const params = new URLSearchParams();
    // Object.keys(getParams).forEach(key=>{
    //     params.append(key,getParams[key]);
    // });
    return axios.delete(`${process.env.REACT_APP_ZILO_URL}/${url}`,{params:getParams}).then(response=>response.data).catch(error=>{
        console.error(error);
        throw error;
    })
}