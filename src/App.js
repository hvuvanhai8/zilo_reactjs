import React from 'react';
import './App.css';
import RouteURL from './Component/RouteURL/RouteURL';
import { BrowserRouter as Router} from "react-router-dom";



function App() {
  return (
    <Router>
      <div className="App">
        <RouteURL />
      </div>
    </Router>
  );
}

export default App;
