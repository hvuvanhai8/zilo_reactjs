import React, { PureComponent } from 'react'
import './footer.css'
import {Link} from "react-router-dom";

class Footer extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            
        }
    }

    render() {
        return (
            <footer id="footer">
            <div className="container main_footer">
                <div className="row row_one">
                <div className="contact">
                    <div className="icon"><i className="fas fa-phone-alt" /></div>
                    <div className="details">
                    <p className="title">Hotline(24/7)</p>
                    <p className="link"><Link to="/">0902 131 474</Link></p>
                    </div>
                </div>
                <div className="contact">
                    <div className="icon"><i className="fas fa-phone-alt" /></div>
                    <div className="details">
                    <p className="title">Chăm sóc khách hàng</p>
                    <p className="link"><Link to="/">0902 131 474</Link></p>
                    </div>
                </div>
                <div className="contact">
                    <div className="icon"><i className="fas fa-envelope-open" /></div>
                    <div className="details">
                    <p className="title">Khiếu nại phản hồi</p>
                    <p className="link"><Link to="/">0902 131 474</Link></p>
                    </div>
                </div>
                <div className="contact">
                    <div className="icon"><i className="fas fa-envelope-open" /></div>
                    <div className="details">
                    <p className="title">Bộ phận kinh doanh</p>
                    <p className="link"><Link to="/">0902 131 474</Link></p>
                    </div>
                </div>
                <div className="contact">
                    <div className="icon"><i className="fas fa-envelope-open" /></div>
                    <div className="details">
                    <p className="title">Bộ phận kinh doanh</p>
                    <p className="link"><Link to="/">0902 131 474</Link></p>
                    </div>
                </div>
                </div>
                <div className="row row_two">
                <ul className="list_info_footer">
                    <li className="address">
                    <Link to="/" className="logo"><img src="images/logo/rv-logo-ft.svg" alt="" /></Link>
                    <p>Số 102 Thái Thịnh, phường Trung Liệt, quận Đống Đa, Hà Nội.</p>
                    <div className="socical_group">
                        <Link to="/"><i className="fab fa-facebook-f" /></Link>
                        <Link to="/"><i className="fab fa-instagram" /></Link>
                        <Link to="/"><i className="fab fa-youtube" /></Link>
                        <Link to="/"><i className="fab fa-linkedin" /></Link>
                        <Link to="/"><i className="fab fa-twitter" /></Link>
                    </div>
                    </li>
                    <li>
                    <span>Công ty</span>
                    <ul>
                        <li><Link to="/">Về Zilo</Link></li>
                        <li><Link to="/">Tuyển dụng</Link></li>
                        <li><Link to="/">Đội ngũ</Link></li>
                        <li><Link to="/">Sàn giao dịch</Link></li>
                        <li><Link to="/">Liên hệ</Link></li>
                    </ul>
                    </li>
                    <li>
                    <span>Dịch vụ</span>
                    <ul>
                        <li><Link to="/">Bán &amp; cho thuê</Link></li>
                        <li><Link to="/">Mua với Zilo</Link></li>
                        <li><Link to="/">Thuê với Zilo</Link></li>
                        <li><Link to="/">Zilo Academy</Link></li>
                        <li><Link to="/">Zilo Agents</Link></li>
                    </ul>
                    </li>
                    <li>
                    <span>Thông tin</span>
                    <ul>
                        <li><Link to="/">Tin tức thị trường</Link></li>
                        <li><Link to="/">Báo cáo phân tích</Link></li>
                        <li><Link to="/">Cập nhật sản phẩm</Link></li>
                        <li><Link to="/">Hướng dẫn sử dụng</Link></li>
                        <li><Link to="/">Kiến thức môi giới</Link></li>
                    </ul>
                    </li>
                    <li>
                    <span>Ứng Dụng</span>
                    <ul>
                        <li><Link to="/">Zilo trên iOS</Link></li>
                        <li><Link to="/">Zilo trên Android</Link></li>
                        <li><Link to="/">Zilo Agents trên iOS</Link></li>
                        <li><Link to="/">Zilo Agents trên Android</Link></li>
                    </ul>
                    </li>
                </ul>
                </div>
                <div className="row row_three">
                <div className="left">
                    <span>Copyright © 2019 - Rever. All Rights Reserved.</span>
                </div>
                <div className="right">
                    <Link to="/">Chính sách bảo mật</Link>
                    <Link to="/">Điều khoản sử dụng</Link>
                    <Link to="/">Phản hồi</Link>
                </div>
                </div>
            </div>
            </footer>

        )
    }
}

export default Footer