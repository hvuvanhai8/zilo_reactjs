import React, { Component } from 'react'
import {Route, Switch} from "react-router-dom";
import PageHome from '../Home/PageHome/PageHome';
import PageList from '../List/PageList/PageList';
import PageDetail from '../Details/PageDetail/PageDetail';


export default class RouteURL extends Component {
    render() {
        return (
            <div>
                <Switch>
                    <Route exact path="/" component={PageHome} />
                    <Route exact path="/:rewrite" component={PageList} />
                    <Route exact path="/:id/:slug.html" component={PageDetail} />
                    <Route component={PageHome} />
                </Switch>
            </div>
        )
    }
}
