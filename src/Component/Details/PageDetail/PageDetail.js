import React, { PureComponent } from 'react'
import HeaderList from '../../HeaderList/HeaderList'
import SlickSlide from '../SlickSlide/SlickSlide'
import MainDetail from '../MainDetail/MainDetail'
import HousingNearby from '../HousingNearby/HousingNearby'
import HousingSame from '../HousingSame/HousingSame'
import Footer from '../../Footer/Footer'

class PageDetail extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            
        }
    }

    
    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {
        
        

        var classifieldId = parseInt(this.props.match.params.id,10);
        
        
        return (
            <div>
                <HeaderList />
                <SlickSlide cla_id={classifieldId}/>
                <MainDetail cla_id={classifieldId} />
                <HousingNearby />
                <HousingSame />
                <Footer />
            </div>
        )
    }
}

export default PageDetail