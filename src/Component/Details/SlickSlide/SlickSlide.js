import React, { PureComponent } from 'react'
import Slider from "react-slick";
import './slickslide.css'
import axios from 'axios';

class SlickSlide extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
          nav1: null,
          nav2: null,
          classifieds: []
        };
      }
    
      componentDidMount() {
        this.setState({
          nav1: this.slider1,
          nav2: this.slider2
        });

        var cla_id = this.props.cla_id;


        axios({
                method: 'GET',
                url: 'https://sosanhnha.com/api/v2/classifieds/'+cla_id,
                data: null
            }).then(res =>{
            // console.log(res);
                this.setState({
                    classifieds: res.data.data
                });
            }).catch(err => {
                console.log(err);
            });
        }

       
        slider_for =() => {
            if(this.state.classifieds.length !== 0){
                var classifieds_images  = this.state.classifieds.picture;
                return classifieds_images.map((e,i) => {
                    return (
                        <div className="item item-for" key={i}>
                            <img src={e.url} alt="" />   
                        </div>
                    )
                })
            }
        }

        slider_nav =() => {
            if(this.state.classifieds.length !== 0){
                var classifieds_images  = this.state.classifieds.picture;
                return classifieds_images.map((e,i) => {
                    return (
                        <div className="item item-nav" key={i}>
                            <img src={e.url} alt="" />   
                        </div>
                    )
                })
            }
        }
    render() {
        const settings = {
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
            ]
        }
        
        
       
        console.log(this.state.classifieds.picture);
    

        return (
            <div className="slides_new">
                <div className="main">
                    <div className="slider slider-for">
                        <Slider
                        asNavFor={this.state.nav2}
                        ref={slider => (this.slider1 = slider)}
                        slidesToShow={3}
                        arrows={false}
                        infinite={false}
                        {...settings}
                        >
                            {
                                this.slider_for()
                            }
                            
                            {/* <div className="item item-for">
                                <img src="https://thietkenhadepmoi.com/wp-content/uploads/2018/02/mau-nha-vuong-dep-2-tang-100m2.jpg" alt="" />
                            </div>
                            <div className="item item-for">
                                <img src="https://angcovat.vn/imagesdata/BT504068/1-hinh-anh-nha-1-tret-1-lau-dep-120m2.jpg" alt="" />
                            </div>
                            <div className="item item-for">
                                <img src="https://img.thuthuatphanmem.vn/uploads/2018/10/09/anh-nha-3d-dep-mat_041507591.jpg" alt="" />
                            </div>
                            <div className="item item-for">
                                <img src="https://i.pinimg.com/originals/52/4a/c2/524ac235d01d976614a81ac4c578fcb6.jpg" alt="" />
                            </div> */}
                        </Slider>
                    </div>
                    
                    <div className="slider slider-nav">
                        <Slider
                        asNavFor={this.state.nav1}
                        ref={slider => (this.slider2 = slider)}
                        slidesToShow={5}
                        swipeToSlide={true}
                        focusOnSelect={true}
                        infinite={false}
                        >
                        {
                            this.slider_nav()
                        }
                        </Slider>
                    </div>
                </div>  
            </div>
            
        );
    }
}

export default SlickSlide