import React, { PureComponent } from 'react'
import Slider from "react-slick";
import './housingsame.css'
import {Link} from "react-router-dom";


class HousingSame extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            
        }
    }

    render() {
        const settings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
            ]
        };
        return (
            <div className="housing_same">
                <div className="container">
                    <div className="row">
                    <div className="col-md-12">
                        <div className="content_housing_same">
                        <h4>Nhà đất tương tự</h4>
                        <div className="same">
                            <Slider {...settings}>
                            <div className="article_same">
                                <div className="cover_image">
                                    <Link to="/">
                                    <img src="/images/house/nhadep.jpg" alt="" />
                                    </Link>
                                </div>
                                <div className="info_project">
                                    <h3><Link to="/" className="name_project">căn hộ The Sun Avenue 3PN, hướng Đông Bắc, không có nội thất, view mé sông</Link></h3>
                                    <div className="address_project"> Mai Chí Thọ, thủ thiêm, quận 2</div>
                                    <ul className="info_house">
                                    <li><i className="fas fa-bed" />3</li>
                                    <li><i className="fas fa-map" />50 m<sup>2</sup></li>
                                    </ul>
                                    <div className="price_house">12 tỷ</div>
                                </div>
                            </div>
                            <div className="article_same">
                            <div className="cover_image">
                                <Link to="/">
                                <img src="/images/house/nhadep.jpg" alt="" />
                                </Link>
                            </div>
                            <div className="info_project">
                                <h3><Link to="/" className="name_project">căn hộ The Sun Avenue 3PN, hướng Đông Bắc, không có nội thất, view mé sông</Link></h3>
                                <div className="address_project"> Mai Chí Thọ, thủ thiêm, quận 2</div>
                                <ul className="info_house">
                                <li><i className="fas fa-bed" />3</li>
                                <li><i className="fas fa-map" />50 m<sup>2</sup></li>
                                </ul>
                                <div className="price_house">12 tỷ</div>
                            </div>
                            </div>
                            <div className="article_same">
                            <div className="cover_image">
                                <Link to="/">
                                <img src="/images/house/nhadep.jpg" alt="" />
                                </Link>
                            </div>
                            <div className="info_project">
                                <h3><Link to="/" className="name_project">căn hộ The Sun Avenue 3PN, hướng Đông Bắc, không có nội thất, view mé sông</Link></h3>
                                <div className="address_project"> Mai Chí Thọ, thủ thiêm, quận 2</div>
                                <ul className="info_house">
                                <li><i className="fas fa-bed" />3</li>
                                <li><i className="fas fa-map" />50 m<sup>2</sup></li>
                                </ul>
                                <div className="price_house">12 tỷ</div>
                            </div>
                            </div>
                            <div className="article_same">
                            <div className="cover_image">
                                <Link to="/">
                                <img src="/images/house/nhadep.jpg" alt="" />
                                </Link>
                            </div>
                            <div className="info_project">
                                <h3><Link to="/" className="name_project">căn hộ The Sun Avenue 3PN, hướng Đông Bắc, không có nội thất, view mé sông</Link></h3>
                                <div className="address_project"> Mai Chí Thọ, thủ thiêm, quận 2</div>
                                <ul className="info_house">
                                <li><i className="fas fa-bed" />3</li>
                                <li><i className="fas fa-map" />50 m<sup>2</sup></li>
                                </ul>
                                <div className="price_house">12 tỷ</div>
                            </div>
                            </div>
                            <div className="article_same">
                            <div className="cover_image">
                                <Link to="/">
                                <img src="/images/house/nhadep.jpg" alt="" />
                                </Link>
                            </div>
                            <div className="info_project">
                                <h3><Link to="/" className="name_project">căn hộ The Sun Avenue 3PN, hướng Đông Bắc, không có nội thất, view mé sông</Link></h3>
                                <div className="address_project"> Mai Chí Thọ, thủ thiêm, quận 2</div>
                                <ul className="info_house">
                                <li><i className="fas fa-bed" />3</li>
                                <li><i className="fas fa-map" />50 m<sup>2</sup></li>
                                </ul>
                                <div className="price_house">12 tỷ</div>
                            </div>
                            </div>
                            </Slider>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default HousingSame