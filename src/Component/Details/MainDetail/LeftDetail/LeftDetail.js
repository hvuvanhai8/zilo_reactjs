import React, { PureComponent } from 'react'
import renderHTML from 'react-render-html';
import './leftdetail.css'
import Iframe from 'react-iframe'
import {Link} from "react-router-dom";


class LeftDetail extends PureComponent {
    static propTypes = {}
    

    constructor(props) {
        super(props)

        this.state = {
            items: [],
            isLoader: false,
        }
    }



    componentDidMount() {
        var cla_id = this.props.cla_id;
        fetch('https://sosanhnha.com/api/v2/classifieds/'+cla_id)
          .then(res => res.json())
          .then(json => {
            this.setState({
              isLoader: true,
              items: json,
            })
        });
        
    }

    render() {
        
        var { isLoader,items } = this.state;
        
    
        if (!isLoader) {
            return <div>Loading...</div>;
        }else{
            return (
                <div className="col-md-8">
                    <div className="top_detail">
                   
                    <div className="box_breadcrumb">
                        <ul>
                        <li><Link to="/">{items.data.address}</Link></li>
                        </ul>
                    </div>
                    <h1>{items.data.title}</h1>
                    <div className="list_info_detail">
                        <ul>
                        <li><i className="fas fa-bed" />3</li>
                        <li><i className="fas fa-bath" />1</li>
                        <li><i className="fas fa-map" />50 m<sup>2</sup></li>
                        <li><i className="fas fa-compass" />Đông nam</li>
                        </ul>
                    </div>
                    </div>
                    <div className="main_detail">
                    <div className="overview">
                        <h4>Tổng Quan</h4>
                        <p>
                        {renderHTML(items.data.description)}
                        </p>
                    </div>
                    <div className="list_info basic_infomation">
                        <h4>Thông tin cơ bản</h4>
                        <ul>
                        <li><p className="left">Loại hình</p><p className="right">Loại khác</p></li>
                        <li><p className="left">Phòng ngủ</p><p className="right">2</p></li>
                        <li><p className="left">Phòng tắm</p><p className="right">1</p></li>
                        <li><p className="left">Diện tích</p><p className="right">70 m2</p></li>
                        <li><p className="left">Diện tích sử dụng</p><p className="right">70 m2</p></li>
                        <li><p className="left">Tình hình nội thất</p><p className="right">Đầy đủ nội thất</p></li>
                        </ul>
                    </div>
                    <div className="list_info utilities">
                        <h4>Tiện ích</h4>
                        <ul>
                        <li><p className="left">Ban công</p><p className="right"><i className="far fa-check" /></p></li>
                        <li><p className="left">Sân vườn</p><p className="right"><i className="far fa-check" /></p></li>
                        <li><p className="left">Chỗ đậu xe hơi</p><p className="right"><i className="far fa-check" /></p></li>
                        <li><p className="left">Góc làm việc</p><p className="right"><i className="far fa-times" /></p></li>
                        <li><p className="left">Hồ bơi riêng</p><p className="right"><i className="far fa-times" /></p></li>
                        <li><p className="left">Phòng giải trí</p><p className="right"><i className="far fa-times" /></p></li>
                        </ul>
                    </div>
                    <div className="list_info furniture">
                        <h4>Tiện ích</h4>
                        <ul>
                        <li><p className="left">Tủ bếp</p><p className="right"><i className="far fa-check" /></p></li>
                        <li><p className="left">Kệ ti vi</p><p className="right"><i className="far fa-check" /></p></li>
                        <li><p className="left">Giường</p><p className="right"><i className="far fa-check" /></p></li>
                        <li><p className="left">Bàn làm việc</p><p className="right"><i className="far fa-times" /></p></li>
                        <li><p className="left">Bàn ăn</p><p className="right"><i className="far fa-times" /></p></li>
                        <li><p className="left">Tủ quần áo</p><p className="right"><i className="far fa-times" /></p></li>
                        </ul>
                    </div>
                    <div className="list_info device_service">
                        <h4>Thiết bị, dịch vụ</h4>
                        <ul>
                        <li><p className="left">Ti vi</p><p className="right"><i className="far fa-check" /></p></li>
                        <li><p className="left">Tủ lạnh</p><p className="right"><i className="far fa-check" /></p></li>
                        <li><p className="left">Máy giặt</p><p className="right"><i className="far fa-check" /></p></li>
                        <li><p className="left">Bếp ga</p><p className="right"><i className="far fa-times" /></p></li>
                        <li><p className="left">Lò vi sóng</p><p className="right"><i className="far fa-times" /></p></li>
                        <li><p className="left">Internet</p><p className="right"><i className="far fa-times" /></p></li>
                        </ul>
                    </div>
                    <div className="advantages_house">
                        <h4>Ưu điểm ngôi nhà</h4>
                        <div className="list_advantages">
                            <div className="advantages_name"><p>ĐẶC ĐIỂM NỔI BẬT</p></div>
                            <div className="advantages_content"><p>Bán nhà Quận 4 diện tích đất 30.2 m2, kết cấu 1 trệt 1 lầu rộng rãi, hẻm trước nhà rộng rãi. Nhà được thiết kế rất đẹp theo phong cách hiện đại, trẻ trung, đầy đủ nội thất cao cấp. Trong khu vực yên tĩnh, an ninh, thích hợp mua để ở.</p></div>
                        </div>
                        <div className="list_advantages">
                            <div className="advantages_name"><p>TÌNH HÌNH NỘI THẤT</p></div>
                            <div className="advantages_content"><p>Nội thất được gia chủ trang bị đầy đủ, có thể dọn đến ở ngay</p></div>
                        </div>
                        <div className="list_advantages">
                            <div className="advantages_name"><p>MÔ TẢ VỊ TRÍ</p></div>
                            <div className="advantages_content"><p>Nhà bán nằm ngay đường 13, Quận 4 thuận tiện di chuyển qua các quận lận cận như Quận 1, Quận 3, Quận 5, Quận 2, Quận 10. Xung quanh có nhiều bệnh viện, trường học, chợ, ngân hàng.</p></div>
                        </div>
                        <div className="list_advantages">
                            <div className="advantages_name"><p>CỘNG ĐỒNG DÂN CƯ</p></div>
                            <div className="advantages_content"><p>Cộng đồng dân cư văn minh, hiện đại. Nằm trong khu vực dân cư lâu năm, nhộn nhịp và sầm uất.</p></div>
                        </div>
                        <div className="list_advantages">
                            <div className="advantages_name"><p>VỀ GIÁO DỤC</p></div>
                            <div className="advantages_content"><p>Xung quanh có nhiều trường học như: Anh ngữ Việt Mỹ, Đại học Luật, Tiểu học Nguyễn Văn Trỗi, Đại học Nguyễn Tất Thành, Tiểu học Xóm Chiểu, v.v.</p></div>
                        </div>
                    </div>
    
                    <div className="images_house">
                        <h4>Hình ảnh</h4>
                        <div className="gallery">
                            <div className="cover">
                            <Link to="/"><img src="/images/house/nhadep.jpg" alt="" /><span>Phòng Khách</span></Link>
                            </div>
                            <div className="cover">
                            <Link to="/"><img src="/images/house/nhadep.jpg" alt="" /><span>Phòng Khách</span></Link>
                            </div>
                            <div className="cover">
                            <Link to="/"><img src="/images/house/nhadep.jpg" alt="" /><span>Phòng Khách</span></Link>
                            </div>
                            <div className="cover">
                            <Link to="/"><img src="/images/house/nhadep.jpg" alt="" /><span>Phòng Khách</span></Link>
                            </div>
                        </div>
                    </div>
    
                    <div className="utility_area map">
                        <h4>Tiện ích khu vực</h4>
                        <div className="zilo_map">
                            <Iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d59587.97785448771!2d105.80194413492788!3d21.02273601629448!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab9bd9861ca1%3A0xe7887f7b72ca17a9!2zSMOgIE7hu5lpLCBIb8OgbiBLaeG6v20sIEjDoCBO4buZaQ!5e0!3m2!1svi!2s!4v1569988303219!5m2!1svi!2s" width="100%" height={450} frameBorder={0} style={{border: 0}} allowFullScreen />
                        </div>
                        <div className="location_map"><strong><i className="fas fa-map-marker-alt" />An Dương, Tây Hồ, Hà Nội</strong></div>
                    </div>
    
                    </div>
                    
                </div>
            )
        }
    }
}

export default LeftDetail