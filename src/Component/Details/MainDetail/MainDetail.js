import React, { PureComponent } from 'react'
import LeftDetail from './LeftDetail/LeftDetail'
import RightDetail from './RightDetail/RightDetail'
import HousingNearby from '../HousingNearby/HousingNearby'
import HousingSame from '../HousingSame/HousingSame'

class MainDetail extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            
        }
    }

    

    render() {
       
        return (
            <div>
                <div className="content_detail_house">
                    <div className="container">
                        <div className="row">
                            <LeftDetail cla_id={this.props.cla_id}/>
                            <RightDetail />
                        </div>
                    </div>
                </div>
                <HousingNearby />
                <HousingSame />
            </div>
        )
    }
}

export default MainDetail