import React, { PureComponent } from 'react'
import './housingnearby.css'
import {Link} from "react-router-dom";

class HousingNearby extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            
        }
    }

    render() {
        return (
            <div className="housing_nearby">
                <div className="container">
                    <div className="row">
                    <div className="col-md-12">
                        <div className="content_housing_nearby">
                        <h4>Nhà đất lân cận</h4>
                        <div className="compare_house">
                            <div className="article">
                            <div className="img"><Link to="/"><img src="/images/house/nhadep.jpg" alt="" /></Link></div>
                            <div className="title">
                                <h3><Link to="/">Nhà phố 4 phòng ngủ, đường số 9, phường 4, Quận 4</Link></h3>
                                <p className="address">Hoàng Diệu, 8, Quận 4, Hồ Chí Minh</p>
                            </div>
                            <div className="status_housing">
                                Tin hết hạn
                            </div>
                            <div className="info">  
                                <ul>
                                <li><i className="fas fa-bed" />3</li>
                                <li><i className="fas fa-bath" />1</li>
                                <li><i className="fas fa-map" />50 m<sup>2</sup></li>
                                <li><i className="fas fa-compass" />Đông nam</li>
                                </ul>
                            </div>
                            </div>
                            <div className="article">
                            <div className="img"><Link to="/"><img src="/images/house/nhadep.jpg" alt="" /></Link></div>
                            <div className="title">
                                <h3><Link to="/">Nhà phố 4 phòng ngủ, đường số 9, phường 4, Quận 4</Link></h3>
                                <p className="address">Hoàng Diệu, 8, Quận 4, Hồ Chí Minh</p>
                            </div>
                            <div className="status_housing">
                                Tin hết hạn
                            </div>
                            <div className="info">  
                                <ul>
                                <li><i className="fas fa-bed" />3</li>
                                <li><i className="fas fa-bath" />1</li>
                                <li><i className="fas fa-map" />50 m<sup>2</sup></li>
                                <li><i className="fas fa-compass" />Đông nam</li>
                                </ul>
                            </div>
                            </div>
                            <div className="article">
                            <div className="img"><Link to="/"><img src="/images/house/nhadep.jpg" alt="" /></Link></div>
                            <div className="title">
                                <h3><Link to="/">Nhà phố 4 phòng ngủ, đường số 9, phường 4, Quận 4</Link></h3>
                                <p className="address">Hoàng Diệu, 8, Quận 4, Hồ Chí Minh</p>
                            </div>
                            <div className="status_housing">
                                Tin hết hạn
                            </div>
                            <div className="info">  
                                <ul>
                                <li><i className="fas fa-bed" />3</li>
                                <li><i className="fas fa-bath" />1</li>
                                <li><i className="fas fa-map" />50 m<sup>2</sup></li>
                                <li><i className="fas fa-compass" />Đông nam</li>
                                </ul>
                            </div>
                            </div>
                            <div className="article">
                            <div className="img"><Link to="/"><img src="/images/house/nhadep.jpg" alt="" /></Link></div>
                            <div className="title">
                                <h3><Link to="/">Nhà phố 4 phòng ngủ, đường số 9, phường 4, Quận 4</Link></h3>
                                <p className="address">Hoàng Diệu, 8, Quận 4, Hồ Chí Minh</p>
                            </div>
                            <div className="status_housing">
                                Tin hết hạn
                            </div>
                            <div className="info">  
                                <ul>
                                <li><i className="fas fa-bed" />3</li>
                                <li><i className="fas fa-bath" />1</li>
                                <li><i className="fas fa-map" />50 m<sup>2</sup></li>
                                <li><i className="fas fa-compass" />Đông nam</li>
                                </ul>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default HousingNearby