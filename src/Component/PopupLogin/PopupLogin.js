import React, { PureComponent } from 'react'
import './popuplogin.css'

class PopupLogin extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            
        }
    }

    render() {
        return (
            <div className="popup_login">
                <div className="overlay" />
                <div className="main_login">
                    <button className="close_login">X</button>
                    <div className="logo_login"><img src="images/logo/logo-r-red.svg" alt="" /></div>
                    <h3>Đăng nhập</h3>
                    <div className="socical ">
                    <a href="#" className="fb_login">Đăng nhập với Facebook</a>
                    <a href="#" className="gg_login">Đăng nhập với Google</a>
                    </div>
                </div>
            </div>

        )
    }
}

export default PopupLogin