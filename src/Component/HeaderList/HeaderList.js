import React, { Component } from 'react'
import './header.css'
import {Link} from "react-router-dom";
import {ApiGet} from "../../api/api.js"

class HeaderList extends Component {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            categories: []
        }
        
    }

    componentDidMount(){
        ApiGet('categories', {fields: 'id,name,rewrite,parent_id,type,name_guest', with: 'all'})
            .then(res => {
                this.setState({
                    categories: res.data
                });
            })
            .catch(error => {
                console.error('errrrr')
            })
    }

    ShowPopUpLoGin = () => {
        alert('Hiển thị popup');
    }

    render() {

        var categories = this.state.categories;

        return (
            <header id="list" className="main_header">
                <div className="col_left">
                    <div className="logo_zilo">
                    <Link to="/"><img src="/images/logo/logo-r-red.svg" alt="" /></Link>
                    <i className="fal fa-chevron-down " id="show_menu" />
                    </div>
                </div>
                <div className="col_between">
                    <div className="search_list">
                    <form action="">
                        <i className="fal fa-search" />
                        <input type="text" className="" name="keyword" placeholder="Tìm theo khu vực, dự án, địa điểm..." autoComplete="off" />
                    </form>
                    </div>
                </div>
                <div className="col_right">
                    <div className="menu_home">
                        <ul>
                            {categories.map((item,i)=>{
                                if(item.id === 38)
                                return (
                                    <li key={i}>
                                    <Link to={"/"+item.rewrite}>{item.name}<i className="fal fa-angle-down" /></Link>
                                        <ul className="menu_child" key={i}>
                                            {categories.map((item,i) => {
                                            if(item.parent_id === 38)
                                                return (
                                                    <li key={i}><Link to={"/"+item.rewrite}>{item.name}</Link></li>
                                                )
                                            })}
                                        </ul>
                                    </li>
                                )
                            })}

                            {categories.map((item,i)=>{
                                if(item.id === 49)
                                return (
                                    <li key={i}>
                                    <Link to={"/"+item.rewrite}>{item.name}<i className="fal fa-angle-down" /></Link>
                                        <ul className="menu_child" key={i}>
                                            {categories.map((item,i) => {
                                            if(item.parent_id === 49)
                                                return (
                                                    <li key={i}><Link to={"/"+item.rewrite}>{item.name}</Link></li>
                                                )
                                            })}
                                        </ul>
                                    </li>
                                )
                            })}
                            
                            
                        
                            <li>
                            <Link to="/">Dự án<i className="fal fa-angle-down" /></Link>
                                <ul className="menu_child">
                                    {categories.map((item,i) => {
                                        if(item.type === "project")
                                            return (
                                                <li key={i}><Link to={"/"+item.rewrite}>{item.name}</Link></li>
                                        )
                                    })}
                                </ul>
                            </li>

                            <li><Link to="/">Nhà - đẹp<i className="fal fa-angle-down" /></Link></li>
                            <li><Link to="/">Cần mua - Cần thuê<i className="fal fa-angle-down" /></Link></li>
                        </ul>
                    </div>
                    <div className="login_home">
                    <Link to="/" className="btn-login" onClick={this.ShowPopUpLoGin}>Đăng nhập</Link>
                    <div className="agency_zilo"><Link to="/">Đăng bài với Zilo</Link></div>
                    </div>
                </div>

                <div className="menu_mobile" id="menu_mb">
                    <div className="login_home">
                    <Link to="/" className="btn-login">Đăng nhập</Link>
                    <div className="agency_zilo"><Link to="/">Đăng bài với Zilo</Link></div>
                    </div>
                    <ul>
                    <li><Link to="/" className="home_menu">Trang chủ</Link></li>
                    <li>
                        <Link to="/" className="link_menu">Bán<i className="fal fa-angle-down" /></Link>
                        <ul className="menu_child">
                        <li><Link to="/">Bán nhà riêng</Link></li>
                        <li><Link to="/">Bán nhà mặt phố</Link></li>
                        <li><Link to="/">Bán đất nền dự án</Link></li>
                        <li><Link to="/">Bán biệt thự liền kề</Link></li>
                        </ul>
                    </li>
                    <li>
                        <Link to="/" className="link_menu">Thuê<i className="fal fa-angle-down" /></Link>
                        <ul className="menu_child">
                        <li><Link to="/">Cho thuê nhà riêng</Link></li>
                        <li><Link to="/">Cho thuê cửa hàng, ki ốt</Link></li>
                        <li><Link to="/">Cho thuê nhà mặt phố</Link></li>
                        <li><Link to="/">Cho thuê văn phòng</Link></li>
                        </ul>
                    </li>
                    <li><Link to="/" className="link_menu">Dự án<i className="fal fa-angle-down" /></Link></li>
                    <li><Link to="/" className="link_menu">Nhà - đẹp<i className="fal fa-angle-down" /></Link></li>
                    <li><Link to="/" className="link_menu">Cần mua - Cần thuê<i className="fal fa-angle-down" /></Link></li>
                    <li><Link to="/" className="link_menu">Cài đặt ứng dụng<i className="fal fa-mobile" /></Link></li>
                    <li><Link to="/" className="phone">Hotline: <strong>0902 131 474</strong></Link></li>
                    </ul>
                </div>
        </header>

        )
    }
}

export default HeaderList