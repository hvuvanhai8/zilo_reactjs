import React, { PureComponent } from 'react'
import MainList from '../MainList/MainList'
import HeaderList from '../../HeaderList/HeaderList'
import TopList from '../TopList/TopList'
import Footer from '../../Footer/Footer'

class PageList extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            
        }
    }


    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {

        // console.log(this.props.match.params.rewrite)
        
        var cat_rewrite = this.props.match.params.rewrite
       

        return (
            <div>
                <HeaderList />
                <TopList />
                <MainList rewrite={cat_rewrite} />
                <Footer />
            </div>
        )
    }
}

export default PageList