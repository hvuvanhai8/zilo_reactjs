import React, { PureComponent } from 'react'
import './mainlist.css'
import {Link} from "react-router-dom";
import {ApiGet} from "../../../api/api.js"

class MainList extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            isLoader: false,
            items: {}
        }
    }

    componentDidMount() {

        var cat_rewite = this.props.rewrite;
        
        
        ApiGet("rewrites?type=detail&slug="+cat_rewite)
            .then(res => {
                
                this.setState({
                    isLoader: true,
                    items: res.data
                });
                
            })
            .catch(error => {
                console.error('errrrr')
            })
        
       
    }


    
    render() {
        var { isLoader,items } = this.state;

        // console.log({ isLoader,items });
       
        if (!isLoader) {
          return <div>Loading...</div>;
        }else{
            return (
                <div className="main_list">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            {items.classifieds.map((item,i) => (
                            <article className="list_view" key={i}>
                                    <div className="image_list">
                                        <Link to={item.id+"/"+item.rewrite+".html"}><img src={item.picture} alt="" /></Link>
                                    </div>
                                    <div className="info_list">
                                        <h3><Link to={item.id+"/"+item.rewrite+".html"} className="list_name">{item.title}</Link></h3>
                                        <div className="list_address"><Link to="/">{item.address}</Link></div>
                                        <div className="list_info">
                                        <ul>
                                            <li><i className="fas fa-bed" />3</li>
                                            <li><i className="fas fa-bath" />1</li>
                                            <li><i className="fas fa-map" />{item.list_acreage} m<sup>2</sup></li>
                                            <li><i className="fas fa-compass" />Đông nam</li>
                                        </ul>
                                        </div>
                                        <div className="action_group">
                                        <div className="customer">
                                            <div className="avatar"><Link to="/"><img src="images/avatar/student.png" alt="" />a</Link></div>
                                            <div className="info_customer">
                                            <h5>{item.contact_name}</h5>
                                            <p>Khách hàng</p>
                                            </div>
                                        </div>
                                        </div>
                                        <div className="price_time">
                                        <span>{item.price}</span>
                                        </div>
                                        <div className="date_time">
                                        {item.date}
                                        </div>
                                    </div>
                            </article>
                            ))};
                        </div>
                    </div>
                </div>
                </div>
    
            )
        }

        
    }
}

export default MainList