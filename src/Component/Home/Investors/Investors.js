import React, { PureComponent } from 'react'
import './investors.css'

class Investors extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            
        }
    }

    render() {
        return (
            <section>
                <div className="container investors">
                    <div className="row"><div className="col-md-12"><h2>Chủ đầu tư nổi bật</h2></div></div>
                    <div className="row"><div className="col-md-12"><p className="des_inves">Thông tin cơ bản và danh mục dự án của các chủ đầu tư bất động sản uy tín hiện nay</p></div></div>
                    <div className="row">
                    <div className="col-md-2 col-6"><div className="image_investor"><img src="images/investors/chudautu_a.jpg" alt="" /></div></div>
                    <div className="col-md-2 col-6"><div className="image_investor"><img src="images/investors/chudautu_b.jpg" alt="" /></div></div>
                    <div className="col-md-2 col-6"><div className="image_investor"><img src="images/investors/chudautu_c.jpg" alt="" /></div></div>
                    <div className="col-md-2 col-6"><div className="image_investor"><img src="images/investors/chudautu_d.jpg" alt="" /></div></div>
                    <div className="col-md-2 col-6"><div className="image_investor"><img src="images/investors/chudautu_e.jpg" alt="" /></div></div>
                    <div className="col-md-2 col-6"><div className="image_investor"><img src="images/investors/chudautu_f.jpg" alt="" /></div></div>
                    <div className="col-md-2 col-6"><div className="image_investor"><img src="images/investors/chudautu_g.jpg" alt="" /></div></div>
                    <div className="col-md-2 col-6"><div className="image_investor"><img src="images/investors/chudautu_h.jpg" alt="" /></div></div>
                    <div className="col-md-2 col-6"><div className="image_investor"><img src="images/investors/chudautu_j.jpg" alt="" /></div></div>
                    <div className="col-md-2 col-6"><div className="image_investor"><img src="images/investors/chudautu_k.jpg" alt="" /></div></div>
                    <div className="col-md-2 col-6"><div className="image_investor"><img src="images/investors/chudautu_l.jpg" alt="" /></div></div>
                    <div className="col-md-2 col-6"><div className="image_investor"><img src="images/investors/chudautu_z.png" alt="" /></div></div>
                    </div>
                </div>
            </section>

        )
    }
}

export default Investors