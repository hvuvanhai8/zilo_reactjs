import React, { PureComponent } from 'react'
import './bannertop.css'

class BannerTop extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            
        }
    }

    render() {
        return (
            <div className="banner_top" style={{background: 'url("images/banner/banner2.jpg") 50% 50% / cover no-repeat'}}>
                <div className="banner_cover">
                    <div className="banner_search">
                    <h1>Tìm kiếm ngồi nhà bạn yêu thích</h1>
                    <label className="lb_active">Mua nhà</label>
                    <label>Thuê nhà</label>
                    <form>
                        <div className="input_search">
                        <i className="fal fa-search" /><input type="text" name="keyword" placeholder="Tìm theo khu vực, dự án, địa điểm..." autoComplete="off" />
                        <button type="submit" className="btn_pc">Tìm kiếm</button>
                        <button type="submit" className="btn_mb"><i className="fal fa-search" /></button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>

        )
    }
}

export default BannerTop