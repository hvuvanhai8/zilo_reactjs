import React, { PureComponent } from 'react'
import HeadderHome from '../../HeaderHome/HeadderHome'
import BannerTop from '../BannerTop/BannerTop'
import SectionProject from '../SectionProject/SectionProject'
import SectionHouse from '../SectionHouse/SectionHouse'
import BrowserHouse from '../BrowseHouse/BrowserHouse'
import Investors from '../Investors/Investors'
import Footer from '../../Footer/Footer'

class MainHome extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            
        }
    }

    render() {
        return (
            <div>
                <HeadderHome />
                <BannerTop />
                <SectionProject />
                <SectionHouse />
                <BrowserHouse />
                <Investors />
                <Footer />
            </div>
        )
    }
}

export default MainHome