import React, { PureComponent } from 'react'
import './sectionproject.css'
import {Link} from "react-router-dom";

class SectionProject extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            
        }
    }

    render() {
        return (
            <section>
            <div className="container project_hot">
                <div className="row"><div className="col-md-12"><h2>Dự án mới nổi bật</h2></div></div>
                <div className="row">
                <div className="col-md-4">
                    <article className="list_article">
                    <div className="cover_image">
                        <Link to="/">
                        <p>Giá tốt</p>
                        <img src="images/projects/project.jpg" alt="" />
                        <div className="mark_cover_image">sắp mở bán</div>
                        </Link>
                    </div>
                    <div className="info_project">
                        <h3><Link to="/" className="name_project">Empire City</Link></h3>
                        <div className="address_project"> Mai Chí Thọ, thủ thiêm, quận 2</div>
                        <div className="price_project"><span>Giá: 25 - 27 tr/m²</span></div>
                    </div>
                    </article>
                </div>
                <div className="col-md-4">
                    <article className="list_article">
                    <div className="cover_image">
                        <Link to="/">
                        <p>Giá tốt</p>
                        <img src="images/projects/project.jpg" alt="" />
                        <div className="mark_cover_image">sắp mở bán</div>
                        </Link>
                    </div>
                    <div className="info_project">
                        <h3><Link to="/" className="name_project">Empire City</Link></h3>
                        <div className="address_project"> Mai Chí Thọ, thủ thiêm, quận 2</div>
                        <div className="price_project"><span>Giá: 25 - 27 tr/m²</span></div>
                    </div>
                    </article>
                </div>
                <div className="col-md-4">
                    <article className="list_article">
                    <div className="cover_image">
                        <Link to="/">
                        <p>Giá tốt</p>
                        <img src="images/projects/project.jpg" alt="" />
                        <div className="mark_cover_image">sắp mở bán</div>
                        </Link>
                    </div>
                    <div className="info_project">
                        <h3><Link to="/" className="name_project">Empire City</Link></h3>
                        <div className="address_project"> Mai Chí Thọ, thủ thiêm, quận 2</div>
                        <div className="price_project"><span>Giá: 25 - 27 tr/m²</span></div>
                    </div>
                    </article>
                </div>
                <div className="col-md-4">
                    <article className="list_article">
                    <div className="cover_image">
                        <Link to="/">
                        <p>Giá tốt</p>
                        <img src="images/projects/project.jpg" alt="" />
                        <div className="mark_cover_image">sắp mở bán</div>
                        </Link>
                    </div>
                    <div className="info_project">
                        <h3><Link to="/" className="name_project">Empire City</Link></h3>
                        <div className="address_project"> Mai Chí Thọ, thủ thiêm, quận 2</div>
                        <div className="price_project"><span>Giá: 25 - 27 tr/m²</span></div>
                    </div>
                    </article>
                </div>
                <div className="col-md-4">
                    <article className="list_article">
                    <div className="cover_image">
                        <Link to="/">
                        <p>Giá tốt</p>
                        <img src="images/projects/project.jpg" alt="" />
                        <div className="mark_cover_image">sắp mở bán</div>
                        </Link>
                    </div>
                    <div className="info_project">
                        <h3><Link to="/" className="name_project">Empire City</Link></h3>
                        <div className="address_project"> Mai Chí Thọ, thủ thiêm, quận 2</div>
                        <div className="price_project"><span>Giá: 25 - 27 tr/m²</span></div>
                    </div>
                    </article>
                </div>
                <div className="col-md-4">
                    <article className="list_article">
                    <div className="cover_image">
                        <Link to="/">
                        <p>Giá tốt</p>
                        <img src="images/projects/project.jpg" alt="" />
                        <div className="mark_cover_image">sắp mở bán</div>
                        </Link>
                    </div>
                    <div className="info_project">
                        <h3><Link to="/" className="name_project">Empire City</Link></h3>
                        <div className="address_project"> Mai Chí Thọ, thủ thiêm, quận 2</div>
                        <div className="price_project"><span>Giá: 25 - 27 tr/m²</span></div>
                    </div>
                    </article>
                </div>
                </div>
                <div className="row"><div className="col-md-12"><Link to="/" className="more">Xem thêm dự án<i className="fal fa-chevron-right" /></Link></div></div>
            </div>
            </section>

        )
    }
}

export default SectionProject