import React, { PureComponent } from 'react'
import MainHome from '../MainHome/MainHome'

class PageHome extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {
        return (
            <div>
                <MainHome />
            </div>
        )
    }
}

export default PageHome