import React, { PureComponent } from 'react'
import './sectionhouse.css'
import {Link} from "react-router-dom";

class SectionHouse extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            items: [],
            isLoader: false,
        }
    }

    componentDidMount() {

        fetch('https://sosanhnha.com/api/v2/classifieds?limit=6&where=cit_id+25')
          .then(res => res.json())
          .then(json => {
            this.setState({
              isLoader: true,
              items: json,
            })
        });
  
    }

    render() {


        var { isLoader,items } = this.state;
        

        if (!isLoader) {
          return <div>Loading...</div>;
        }else{
            return (
                <section>
                <div className="container section_house">
                    <div className="row"><div className="col-md-12"><h2>Bán nhà tại Hà Nội</h2></div></div>
                        <div className="row">
                            {items.data.classifieds.map((item,i) => (
                            <div className="col-md-4" key={i}>
                                <article className="list_article">
                                    <div className="cover_image">
                                        <Link to={item.id+"/"+item.rewrite+".html"}>
                                        <img src={item.picture} alt="" />
                                        </Link>
                                    </div>
                                    <div className="info_project">
                                        <h3><Link to={item.id+"/"+item.rewrite+".html"} className="name_project">{item.title}</Link></h3>
                                        <div className="address_project">{item.address}</div>
                                        <ul className="info_house">
                                        <li><i className="fas fa-bed" />3</li>
                                        <li><i className="fas fa-map" />{item.list_acreage} m<sup>2</sup></li>
                                        </ul>
                                        <div className="price_house">{item.price}</div>
                                    </div>
                                </article>
                            </div>
                            ))};
                    </div>
                    <div className="row"><div className="col-md-12"><Link to="/" className="more">Xem thêm nhà đất<i className="fal fa-chevron-right" /></Link></div></div>
                </div>
                </section>
    
            )
        }

       
    }
}

export default SectionHouse