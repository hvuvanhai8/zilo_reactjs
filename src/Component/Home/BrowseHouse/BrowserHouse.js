import React, { PureComponent } from 'react'
import './browserhouse.css'
import {Link} from "react-router-dom";

class BrowserHouse extends PureComponent {
    static propTypes = {}

    constructor(props) {
        super(props)

        this.state = {
            
        }
    }

    render() {
        return (
            <section>
        <div className="container browser_house">
            <div className="row"><div className="col-md-12"><h2>Xem nhà đất theo địa điểm</h2></div></div>
            <div className="row">
            <div className="col-md-3 col-6">
                <ul className="col_house">
                <li><Link to="/">Bán nhà Quận 1</Link></li>
                <li><Link to="/">Bán nhà Quận 1</Link></li>
                <li><Link to="/">Bán nhà Quận 1</Link></li>
                <li><Link to="/">Bán nhà Quận 1</Link></li>
                <li><Link to="/">Bán nhà Quận 1</Link></li>
                <li><Link to="/">Bán nhà Quận 1</Link></li>
                <li><Link to="/">Bán nhà Quận 1</Link></li>
                <li><Link to="/">Bán nhà Quận 1</Link></li>
                <li><Link to="/">Bán nhà Quận 1</Link></li>
                <li><Link to="/">Bán nhà Quận 1</Link></li>
                </ul>
            </div>
            <div className="col-md-3 col-6">
                <ul className="col_house">
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                </ul>
            </div>
            <div className="col-md-3 col-6">
                <ul className="col_house">
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                </ul>
            </div>
            <div className="col-md-3 col-6">
                <ul className="col_house">
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                <li><Link to="/">Cho thuê Quận 1</Link></li>
                </ul>
            </div>
            </div>
        </div>
        </section>

        )
    }
}

export default BrowserHouse