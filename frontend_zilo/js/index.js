$(document).ready(function(){
    $( "#show_menu" ).click(function() {
        if(!$('.logo_zilo > i').hasClass('rotate')){
            $("#show_menu").css("transform","rotate(-180deg)");
            $("#show_menu").addClass('rotate');
        }else{
            $("#show_menu").css("transform","unset");
            $("#show_menu").removeClass('rotate');
        }
        $( "#menu_mb" ).toggle("500");
        
    });

    $(".menu_mobile>ul>li").click(function(){
        if(!$('.link_menu > i').hasClass('rotate')){
            $(this).find(".link_menu > i").css("transform","rotate(-180deg)");
            $(this).find(".link_menu > i").addClass('rotate');
        }else{
            $(this).find(".link_menu > i").css("transform","unset");
            $(this).find(".link_menu > i").removeClass('rotate');
        }
        $(this).children(".menu_child").toggle("100");
    });
});

function getLogin(){
    $('.popup_login').fadeIn(200);
}

$(document).ready(function () {
    $('.close_login').click(function() {
        $('.popup_login').fadeOut(200);
    })

    $('.overlay').click(function() {
        $('.popup_login').fadeOut(200);
    })




    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
      });
      $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: true,
        focusOnSelect: true
    });


    $('.same').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

});


